const mysql = require('mysql'); //modulo de mysql

const { database } = require('./keys'); //importamos la configuracion de mi base de datos

const pool = mysql.createPool(database); // son hilos que se ejecutan para tareas a la vez

pool.getConnection((err, connections) => {
    if (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error("LA conexion de la base de daatos fue cerrada");
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.error("muchas conexiones en la base de datos");
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('Conexion rechazada');
        }
    }
    if (connections) {
        connections.release();
        console.log('La base de esta conectada');
        return;
    }
});
module.exports = pool;