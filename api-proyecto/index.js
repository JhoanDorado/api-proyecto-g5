const express = require('express');
const app = express();
app.use(express.json()); //para que las request sean de tipo jason
const dbconect = require('./database/database')

//configuracion del puerto
app.set('port', process.env.PORT || 4000);

//iniciar el servidor
app.listen(app.get('port'), () => {
    console.log('Server en el puerto', app.get('port'));
});


//url buscar un usuario en especifico 
app.get('/api/usuario/:pk_user', async(req, res) => {

    await dbconect.query('Select * from usuario where  ? and  ?', [req.params, req.body], (err, filas) => {
        if (err) {
            throw err;
        } else {
            console.log(filas)
            res.send(filas);
        }
    });

})


//url agregar usuario
app.post('/api/usuarios', async(req, res) => {
    const { fullname, pk_user, password, gmail } = req.body;
    const newUser = { fullname, pk_user, password, gmail };

    const { ip, ciudad, peso, edad, fk_usuario_in } = req.body;
    const newInfo = { ip, ciudad, peso, edad, fk_usuario_in };
    try {
        await dbconect.query('INSERT INTO usuario set ?', [newUser])
        await dbconect.query('INSERT INTO informacion set ?', [newInfo], (err, filas) => {
            if (err) {
                throw err;
            } else {
                console.log(filas)
                res.send(filas);
            }
        })

    } catch (error) {
        console.log(error)
    }
});

//url que obtiene todos los usuarios y la informacion
app.get('/api/usuarios', async(req, res) => {
    await dbconect.query('Select * from usuario as u, informacion as i where u.pk_user=i.fk_usuario_in', (err, filas) => {
        if (err) {
            throw err;
        } else {
            console.log(filas)
            res.send(filas);
        }
    });
});


//apis para los graficos
//url que obtiene todas las ciudades con el numero de aplicaciones usadas
app.get('/api/read/mapa', (req, res) => {
    dbconect.query('SELECT i.ciudad ,COUNT(p.name_app) AS cantidad from control_app as p, informacion as i  where p.fk_ip = i.ip GROUP BY i.ciudad', (err, filas) => {
        if (err) {
            throw err;
        } else {
            res.send(filas);
        }
    });

});

//url que obtiene las apps usadas de una region o en general
app.get('/read/controlApp/:ciudad', async(req, res) => {
    const { ciudad } = req.params;
    if (ciudad == 'nulo') {
        await dbconect.query("SELECT name_app ,COUNT(name_app) AS cantidad from control_app  GROUP BY name_app;", (err, filas) => {
            if (err) {
                throw err;
            } else {
                res.send(filas);
            }
        });
    } else {
        await dbconect.query("SELECT p.name_app ,COUNT(p.name_app) AS cantidad from control_app as p, informacion as i  where p.fk_ip = i.ip and i.ciudad=?  GROUP BY p.name_app;", [ciudad], (err, filas) => {
            if (err) {
                throw err;
            } else {
                res.send(filas);
            }
        });
    }
});